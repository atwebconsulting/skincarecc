window.addEventListener('load', function () {

    // obtain cookieconsent plugin
    var cookieconsent = initCookieConsent();

    // run plugin with config object
    cookieconsent.run({
        autorun: true,
        revision: 1,
        current_lang: document.documentElement.getAttribute('lang'),
        theme_css: '/assets/css/cookieconsent.css?v=2.1',
        autoclear_cookies: true,
        page_scripts: true,

        gui_options: {
            consent_modal: {
                layout: 'box',                // box,cloud,bar
                position: 'middle center',           // bottom,middle,top + left,right,center
                transition: 'zoom'           // zoom,slide
            },
            settings_modal: {
                layout: 'box',                // box,bar
                transition: 'slide'           // zoom,slide
            }
        },

        onAccept: function (cookie) {
            
            dataLayer.push({'event': 'update', 'cookie': JSON.stringify(cookie)});
            console.log("PUSH UPDATE (onAccept)!");
        },

        onChange: function (cookie, changed_preferences) {
            dataLayer.push({'event': 'update', 'cookie': JSON.stringify(cookie)});
            console.log("PUSH UPDATE (onChange)!");
        },

        languages: {
            cs: {
                consent_modal: {
                    title: 'S cookies se žije lépe',
                    description: 'Aby pro vás bylo prohlížení našich stránek příjemnější a byli jste stále v obraze o našich službách, používáme různé cookies. Jsou to jak nutné cookies (nezbytné pro správné fungování webu), tak analytické (pomáhající nám zlepšovat náš web dle vašich preferencí) a marketingové (umožňující nám oslovit vás relevantní nabídkou našich služeb na stránkách partnerů jako je Facebook nebo Seznam).<br><br> K jejich využití potřebujeme váš souhlas, který dáte kliknutím na „Přijmout všechny“. Svůj souhlas můžete kdykoliv odvolat, příp. nastavit souhlas jen s některými z nich kliknutím na „Nastavit souhlasy“, či <a id="nastavitnutne" type="button"  class="cc-link">přijmout jen nezbytné cookies</a>.',
                    primary_btn: {
                        text: 'Přijmout všechny',
                        role: 'accept_all'              // 'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Nastavit specificky',
                        role: 'settings'                // 'settings' or 'accept_necessary'
                    }
               },
                settings_modal: {
                    title: 'Nastavení Cookies',
                    save_settings_btn: 'Uložit nastavení',
                    accept_all_btn: 'Přijmout vše',
                    cookie_table_headers: [
                        {col1: 'Jméno'},
                        {col2: 'Doména/Služba'},
                        {col3: 'Expirace'},
                    ],
                    blocks: [
                        {
                            title: 'Použité cookie',
                            description: 'Na webu používáme cookies. Můžete si vybrat, které cookies povolíte.'
                        }, {
                            title: 'Nutné cookies',
                            description: 'Tyto cookies jsou opravdu potřebné pro základní funkčnost webu. Bez nich by web nemusel fungovat správně.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            },
                           
                        }, 
                        {
                            title: 'Analytické cookies',
                            description: 'Tyto cookies sbírají informace o průchodu návštěvníka našimi stránkami . Všechny tyto cookies jsou anonymní a nemohu návštěvníka nijak identifikovat. Tyto cookies nejsou nutné pro fungování webu jako takového, ale pomáhají nám web pro vás neustále vylepšovat.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
                           
                        },
                        {
                            title: 'Marketingové cookies',
                            description: 'Cílem těchto cookies je propojit náš web se sociálními a reklamními sítěmi třetích stran jako například Facebook nebo Google Ads. Díky tomuto propojení vám u těchto partnerů může být zobrazována relevantní reklama.',
                            toggle: {
                                value: 'ads',
                                enabled: false,
                                readonly: false
                            },
                            
                        },                       
                       
                    ]
                }
            },
            en: {
                consent_modal: {
                    title: 'Cookies give us better experience',
                    description: 'In order to make your experience with this website and our services more pleasant, we use cookies. Toghether with cookies, that are necessary a proper basic functions of the website, we use mainly analytical cookies, that are designed to improve our services to you, our visitors. Also, the website usesmarketing cookies that can help improve your advertising experience on various partner sites. In order use cookies, we need your consent, that you can provide us by clicking on "Accept all". You can revoke your consent at any time, set specific consents only by clicking on "Specific settings" or you can <a id="nastavitnutne" type="button"  class="cc-link">accept only necessary cookies</a>.',
                    primary_btn: {
                        text: 'Accept all',
                        role: 'accept_all'              // 'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Specific settings',
                        role: 'settings'                // 'settings' or 'accept_necessary'
                    }
               },
                settings_modal: {
                    title: 'Cookie settings',
                    save_settings_btn: 'Save settings',
                    accept_all_btn: 'Accept all',
                    cookie_table_headers: [
                        {col1: 'Name'},
                        {col2: 'Domain'},
                        {col3: 'Expiration'},
                    ],
                    blocks: [
                        {
                            title: 'Cookies that we use',
                            description: 'We use cookies on this website. You can choose which cookies you want to enable to be tracked.'
                        }, {
                            title: 'Necessary cookies',
                            description: 'These cookies are necessary for the basic functionality of the website. Without them, website would not work properly.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            }
                        }, {
                            title: 'Analytical cookies',
                            description: 'These cookies collect information about the visitor\'s passage through our site. All these cookies are anonymous and cannot identify the personality of visitor in any way. These cookies help to continuously improve the visitor experience of this website.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
                            
                        },
                        {
                            title: 'Advertising cookies',
                            description: 'These cookies are meant to improve your advertising experience on partner sites and allow us to address our visitors with a relevant informatio about our services on partner sites.',
                            toggle: {
                                value: 'ads',
                                enabled: false,
                                readonly: false
                            },
                           
                        },                        
                       
                    ]
                }
            },
            ru: {
                consent_modal: {
                    title: 'Файлы cookie улучшают жизнь',
                    description: 'Чтобы сделать просмотр нашего сайта более удобным и предоставить вам актуальную информацию о наших услугах, мы используем различные файлы cookie. Строго необходимые файлы cookie обеспечивают надлежащую работу сайта, аналитические cookie помогают нам совершенствовать сайт в зависимости от ваших предпочтений, а рекламные файлы cookie позволяют нам адресно предлагать вам свои услуги на партнерских сайтах (например, Facebook или Seznam). Для их использования нам необходимо ваше согласие, которое подтверждается нажатием на кнопку «Согласиться с использованием всех файлов cookie».  Вы можете в любой момент отозвать свое согласие, дать разрешение на использование лишь некоторых файлов cookie, нажав на «Настройки файлов cookie», или <a id="nastavitnutne" type="button"  class="cc-link">согласиться с использованием только необходимых файлов cookie</a>.',
                    primary_btn: {
                        text: 'Принять все файлы cookie',
                        role: 'accept_all'              // 'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Настройки файлов cookie',
                        role: 'settings'                // 'settings' or 'accept_necessary'
                    }
               },
                settings_modal: {
                    title: 'Настройки файлов cookie',
                    save_settings_btn: 'Сохранить настройки',
                    accept_all_btn: 'Принять все файлы cookie',
                    cookie_table_headers: [
                        {col1: 'Name'},
                        {col2: 'Domain'},
                        {col3: 'Expiration'},
                    ],
                    blocks: [
                        {
                            title: 'Файлы cookie, используемые на этом сайте',
                            description: 'На данном сайте применяются файлы cookie. Вы можете настроить параметры их использования.'
                        }, {
                            title: 'Строго необходимые файлы cookie',
                            description: 'Эти файлы cookie действительно необходимы для обеспечения базовой функциональности веб-сайта. Без них сайт, скорее всего, не будет работать исправно.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            }
                        }, {
                            title: 'Аналитические файлы cookie',
                            description: 'Эти файлы cookie собирают сведения о том, как пользователи перемещаются по нашему сайту. Вся собираемая информация является анонимной и не позволяет идентифицировать пользователя. Эти файлы не влияют на работу сайта как такового, но помогают нам постоянно его совершенствовать.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
                            
                        },
                        {
                            title: 'Рекламные файлы cookie',
                            description: 'Задача рекламных файлов cookie — связать наш сайт со сторонними социальными и рекламными сетями, такими как Facebook или Google Ads, благодаря чему на сайтах этих партнеров вам может показываться контекстная реклама.',
                            toggle: {
                                value: 'ads',
                                enabled: false,
                                readonly: false
                            },
                           
                        },                        
                       
                    ]
                }
            },
            pl: {
                consent_modal: {
                    title: 'Z cookies żyje się lepiej',
                    description: 'Aby przyjemniej przeglądało się Państwu nasze strony internetowe oraz by byli Państwo na bieżąco z naszymi usługami, korzystamy z różnorodnych cookies. Obowiązkowe cookies są konieczne do prawidłowego działania strony internetowej, cookies analityczne pomagają nam dostosować naszą stronę do Państwa preferencji, a cookies marketingowe pozwalają nam kierować do Państwa dostosowaną ofertę naszych usług na stronach partnerów (np. Facebook lub Seznam). Abyśmy mogli z nich korzystać, potrzebujemy Państwa zgody, której można udzielić, klikając przycisk Przyjąć wszystkie. W dowolnym momencie mogą Państwo wycofać swoją zgodę lub <a id="nastavitnutne" type="button"  class="cc-link">wyrazić zgodę na tylko obowiązkowe cookies</a>.',
                    primary_btn: {
                        text: 'Przyjąć wszystkie',
                        role: 'accept_all'              // 'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Konfiguracja zgód',
                        role: 'settings'                // 'settings' or 'accept_necessary'
                    }
               },
                settings_modal: {
                    title: 'Ustawienia plików cookies',
                    save_settings_btn: 'Zapisz ustawienia',
                    accept_all_btn: 'Przyjąć wszystkie',
                    cookie_table_headers: [
                        {col1: 'Name'},
                        {col2: 'Domain'},
                        {col3: 'Expiration'},
                    ],
                    blocks: [
                        {
                            title: 'Wykorzystywane pliki cookies',
                            description: 'Na naszej stronie internetowej korzystamy z plików cookies. Mogą Państwo wybrać, na które pliki wyrażają Państwo zgodę.'
                        }, {
                            title: 'Obowiązkowe pliki cookies',
                            description: 'Te pliki cookies są naprawdę konieczne do podstawowego funkcjonowania strony internetowej. Bez nich strona internetowa mogłaby nie działać prawidłowo.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            }
                        }, {
                            title: 'Analityczne pliki cookies',
                            description: 'Te pliki cookies gromadzą informacje na temat przepływu odwiedzających przez nasze strony. Analityczne pliki cookies są anonimowe i nie można na ich podstawie w żaden sposób zidentyfikować odwiedzającego. Nie są konieczne do samego działania strony internetowej, ale pomagają nam stale poprawiać działanie strony.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
                            
                        },
                        {
                            title: 'Marketingowe pliki cookies',
                            description: 'Celem marketingowych plików cookies jest powiązanie naszej strony internetowej z serwisami społecznościowymi i reklamowymi podmiotów zewnętrznych, takich jak Facebook lub Google Ads. Takie powiązanie sprawia, że mogą być Państwu wyświetlane adekwatne reklamy u tych partnerów.',
                            toggle: {
                                value: 'ads',
                                enabled: false,
                                readonly: false
                            },
                           
                        },                        
                        
                    ]
                }
            },
            de: 
            {
                consent_modal: {
                    title: 'Mit Cookies lebt man besser',
                    description: 'Um Ihren Besuch auf der Website nutzerfreundlicher zu gestalten und Sie über unsere Dienste auf dem Laufenden zu halten, verwenden wir verschiedene Cookies. Dabei handelt es sich sowohl um essenzielle Cookies (die für die richtige Funktionsweise der Website unerlässlich sind) als auch um analytische Cookies (die uns helfen, unsere Website entsprechend Ihren Präferenzen attraktiver zu gestalten) sowie Marketing-Cookies (mit denen wir Sie mit einem entsprechenden Angebot unserer Dienstleistungen auf den Partnerseiten, wie Facebook oder Seznam, ansprechen können). Für die Verwendung dieser Cookies benötigen wir Ihre Zustimmung, die Sie durch das Anklicken von „Alle akzeptieren“ erteilen. Sie können Ihre Einwilligung jederzeit widerrufen, bzw. nur bestimmte Cookies auswählen, indem Sie auf „Individuelle Datenschutzeinstellungen“ klicken, oder <a id="nastavitnutne" type="button"  class="cc-link">nur notwendige Cookies akzeptieren</a>.',
                    primary_btn: {
                        text: 'Alle akzeptieren',
                        role: 'accept_all'              // 'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Spezifische Einwilligungen',
                        role: 'settings'                // 'settings' or 'accept_necessary'
                    }
               },
                settings_modal: {
                    title: 'Cookie-Einstellungen',
                    save_settings_btn: 'Einstellungen speichern',
                    accept_all_btn: 'Alle akzeptieren',
                    cookie_table_headers: [
                        {col1: 'Name'},
                        {col2: 'Domain'},
                        {col3: 'Ablaufdatum'},
                    ],
                    blocks: [
                        {
                            title: 'Verwendete Cookies',
                            description: 'Diese Website verwendet Cookies. Sie können festlegen, welche Cookies Sie zulassen möchten.'
                        }, {
                            title: 'Essenzielle Cookies',
                            description: 'Diese Cookies sind für die grundlegende Funktionalität der Website erforderlich. Ohne sie funktioniert die Website möglicherweise nicht richtig.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            }
                        }, {
                            title: 'Analytische Cookies',
                            description: 'Diese Cookies sammeln Informationen über die Nutzungsweise unserer Website. Alle diese Cookies sind anonym und können den Besucher in keiner Weise identifizieren. Diese Cookies sind für das Funktionieren der Website als solches nicht erforderlich, sie helfen uns aber, die Website kontinuierlich zu verbessern.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
                            
                        }, 
                        {
                            title: 'Marketing-Cookies',
                            description: 'Diese Cookies ermöglichen es uns, unsere Besucher mit entsprechenden Angeboten für unsere Dienstleistungen auf Partnerseiten, wie Facebook oder Seznam, anzusprechen.',
                            toggle: {
                                value: 'ads',
                                enabled: false,
                                readonly: false
                            },
                           
                        },                       
                        
                    ]
                }
            }
            
        }
    });
    document.getElementById("nastavitnutne").onclick = function () {
        cookieconsent.hide();
        cookieconsent.accept([]); 
    };

}); 	